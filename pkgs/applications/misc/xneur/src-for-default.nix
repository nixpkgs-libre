rec {
   version="0.11.1";
   name="xneur-0.11.1";
   hash="12r2wv1glnx3ilqkrypff9r3mxzk1m3yma3khmam1b0z32lfbxxx";
   url="http://dists.xneur.ru/release-${version}/tgz/xneur-${version}.tar.bz2";
   advertisedUrl="http://dists.xneur.ru/release-0.11.1/tgz/xneur-0.11.1.tar.bz2";
  
  
}
