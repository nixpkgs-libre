rec {
   version="0.5.4";
   name="xmoto-0.5.4";
   hash="1r548hx7yqxql5b0d1byxbwxjhlss063vsj5xy9cc1b8dqhmkqh5";
   url="http://download.tuxfamily.org/xmoto/xmoto/${version}/xmoto-${version}-src.tar.gz";
   advertisedUrl="http://download.tuxfamily.org/xmoto/xmoto/0.5.4/xmoto-0.5.4-src.tar.gz";
  
  
}
