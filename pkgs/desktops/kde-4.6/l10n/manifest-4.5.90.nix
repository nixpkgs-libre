[
{
  lang = "ar";
  sha256 = "19mfcq1ms0k6msir8q1g70myvqiapcf6a2r8wq47achxnpkicia9";
}
{
  lang = "bg";
  sha256 = "1njbxf5y3pl9nmyj913qinaa3qr4q4v8gvwmg7mfidddrr60yniz";
}
{
  lang = "ca";
  sha256 = "1yv5220q1d3w4nmix9jzz98fhyk0516v3mr5j4pczf407mrggh3l";
}
{
  lang = "ca@valencia";
  sha256 = "047rq1g0a7dy0kdna9gd4x906dk6inb61vw0xl6kkb3kzxxp3das";
}
{
  lang = "cs";
  sha256 = "1cazhfrh9nagxi2grlwldwl4j92xhgj8lmyaxkhb893nj845z108";
}
{
  lang = "da";
  sha256 = "1cm19gp0qiq6yqdzwpv14swj8g7rkvspqgnn5b2i5f3qqr25d29g";
}
{
  lang = "de";
  sha256 = "0sr51gpyfdgai8frrr0dw7qyssc39kl3fqkjg8w8zjlsp5x6ddx5";
}
{
  lang = "el";
  sha256 = "0s6mg2diy84my66zgcs5z8xk13j2a8q71xkrvw4d1z8pbnbw4i66";
}
{
  lang = "en_GB";
  sha256 = "1n461w69gs9q86kp6xy0980ziry61idr0bk614mv58ip1myzwhgm";
}
{
  lang = "eo";
  sha256 = "1s7p0d45f30871rikx74rc238z9w239pdb4ay9nfx2618wnsrhkr";
}
{
  lang = "es";
  sha256 = "1583gkwac4ys2sz8a93km0fabqpasslas5jzkavbz1bn8ps1q0w3";
}
{
  lang = "et";
  sha256 = "00sbnyqhvsdy9y1f59ir0mpxbj6h5x7gc8hiigfzsymlm14fhf26";
}
{
  lang = "eu";
  sha256 = "1pk6dmakxzj3qqq506flyipcc1sc954lsf4zsy82ii8ki06zvrvw";
}
{
  lang = "fi";
  sha256 = "1xcdmmvf8x723ngw5xw714x86xpn66qb3pw6h0x01mlh28lprq7j";
}
{
  lang = "fr";
  sha256 = "18wbz82cnxk51vj4qpw54xcrplga7vpm7p0cw19cjwph2xs62bjl";
}
{
  lang = "fy";
  sha256 = "1n3ix81jz9716ai9xm99r2v3n64w0kqvr8iz58zlrns5k9cig1nw";
}
{
  lang = "ga";
  sha256 = "0gm5ylr0dw6slilf1xdsksx17f7kqxmagdywl3dp2fry2h6z3xrn";
}
{
  lang = "gl";
  sha256 = "132bchxzm4qkbjcf5ljwniyjiahfwkd3i01sqm57l9ssgkq88f1r";
}
{
  lang = "gu";
  sha256 = "1sg4c0ijap8aird7fwflzw2w7yhx08b8baw4453nkk28s3l0wbl8";
}
{
  lang = "he";
  sha256 = "1gjngdh6qwmwfhyxcad1xsyq0l25v88bdg4yl1m5iw5a44rvn9br";
}
{
  lang = "hi";
  sha256 = "1q5rhiwn5fp8sgdrlqh2gh1f4q60x0s5x79q31fnw4kdk5akkrbs";
}
{
  lang = "hr";
  sha256 = "0hsdh8pnir8ykdsakbii7jj2nd9g1h03s29bmbg6w4r7q39xif7i";
}
{
  lang = "hu";
  sha256 = "0zpmndlb0vp3sp7z8g233335kak3ms8gzzgn1dhqch0rs03dx9cb";
}
{
  lang = "ia";
  sha256 = "01jrkfs4jp8z4554sia73vk4js1sd32a3swhal664vdwnf62cw70";
}
{
  lang = "id";
  sha256 = "0nylab5ncqdx5s3nnq7lx3fd70crjwihkgp9hv689vz4l8g1si4z";
}
{
  lang = "is";
  sha256 = "1rj8m2qsfhrv4k99mf5sydz5mhq0anhcfi4na04l1hi7icypbz9d";
}
{
  lang = "it";
  sha256 = "1f1kd82f3pc4szbfwsyxpn115rp2fbkx98gzxpnaggb3j88vq2k9";
}
{
  lang = "ja";
  sha256 = "0axl759bpv18dl0pw52hlksgdza1y8f7jfw5zgv6rc9qks1f1f9q";
}
{
  lang = "kk";
  sha256 = "14xvnj21kqp41ayi6n1a8qbph5llf9izs5qql82lcvph3a7zdjxv";
}
{
  lang = "km";
  sha256 = "0fywr8c6cz0wp4kx56xfd93mjbgwmnmh6zv0kdq8lr9xzjfjbbvq";
}
{
  lang = "kn";
  sha256 = "13isz33r2wkir2n53fxvmadk5fp4178rxm3nlr8c0l4fqic5pj1a";
}
{
  lang = "ko";
  sha256 = "1l18crb73cg5an8isgv0iih1mc3j4ghqnfrx39bbrl5ljym78xfx";
}
{
  lang = "lt";
  sha256 = "01b50dzhxlczmjqc07ph40bz86020ll1ddzcr0285dqqxcl466hg";
}
{
  lang = "lv";
  sha256 = "0vcbl1laf0vnpks3mfghxffvnyyl4av4lgkgx9annw625aybwc5w";
}
{
  lang = "ml";
  sha256 = "06z3ihbd3mjzmr68h5bxnp2zhkfnjx7a4ldkv9hj1m4miwpa9af6";
}
{
  lang = "nb";
  sha256 = "1zp7803s50y750i59cpfq5n4m4yq7xjyqzrrajk4k2cp3kv7prv0";
}
{
  lang = "nds";
  sha256 = "1f71gl6v9nasrci17glbllyc1h8iaw69j1vf36pyzzp3jwi6y6pg";
}
{
  lang = "nl";
  sha256 = "18x4dfffyc11acl90bj5d86xvbjiq9bkszdgnmninaa6zjmmvng9";
}
{
  lang = "nn";
  sha256 = "0syb3x2mlz7lf6awa1h0lchj7qmwmdmxai2cy0h0ja3jvbh6hrsw";
}
{
  lang = "pa";
  sha256 = "1dd5bpn409b839blb5zj27gc0mg13r3d36ap80lybivqpga0c5bw";
}
{
  lang = "pl";
  sha256 = "0cmqh4f846s910jlhgc1vpzwlbry0vc4wwpx5misiybh2didhly9";
}
{
  lang = "pt";
  sha256 = "01z1bz56r1jpfwl2vhfkb8w667layv2hm163y6x4i940qfiig167";
}
{
  lang = "pt_BR";
  sha256 = "0swilj7qvsrg66xccjmyzlj4l6k88qghvdcs6m9nha3rv199p35k";
}
{
  lang = "ro";
  sha256 = "08c8ikxfmxgiwchdhayjflk4773rfan2jmjnbrixmam21x2py5ys";
}
{
  lang = "ru";
  sha256 = "01nskj9mi28xbg77shnmhc46ilx9qrnsv1rsmpblphqxs3wczpqy";
}
{
  lang = "sk";
  sha256 = "12ja66s0v85d8w5nk1c0xwfs04wq0ngxmpypc5igky90qxhg3f75";
}
{
  lang = "sl";
  sha256 = "0qqass959xcc7gg1q76w10alrj1ypxc5hg43hlbjagxy8x1nkd99";
}
{
  lang = "sr";
  sha256 = "16cq9iz2njvwm4d57f07ay4rzw66pwphblxrdrlk22fvkli1hmps";
}
{
  lang = "sv";
  sha256 = "05hskmbmr9gzfk7i3xlh6a56inmjgl26cvbkcm967mayfigxcaqp";
}
{
  lang = "th";
  sha256 = "0n9jbngj3l1crqgqv9am8bl3l3sa3x2ccna4p9ccbjyvl4xgryij";
}
{
  lang = "tr";
  sha256 = "14lfp80r9vwr4kbway2kzff3qr9dgywal6n68adam7jhagfmxifc";
}
{
  lang = "uk";
  sha256 = "1ygac3jnjw4y4jfk4r8hba1d9ppb41sq50szbh4s4cw0p77kwi91";
}
{
  lang = "wa";
  sha256 = "1pg37wyyc2fbjy1pmmk8vchk3jl0x9qahqa8bka3w24piq8k2bal";
}
{
  lang = "zh_CN";
  sha256 = "1664vjjs2nfldmzwda5p2qv9gayx1jpnr0glm87whgak1yyhwjsb";
}
{
  lang = "zh_TW";
  sha256 = "05c3m128c92gj6n0jr6d6wwbs142nwyxxj6bf0rq24pdl1m5fdn8";
}
]
