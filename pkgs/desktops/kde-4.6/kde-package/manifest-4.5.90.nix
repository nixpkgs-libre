{
  stable = false;
  packages = builtins.listToAttrs [
    {
      name = "kdeaccessibility";
      value = "0jqfyzy3jvzyv1b6n1r589vk7qs8phng6az5jgsknhjjrhsn40hy";
    }
    {
      name = "kdeadmin";
      value = "1kk0xzf6mrhm946rsrpizzpkx59rf8xzxfj5h5lkcrajmq4xr0q8";
    }
    {
      name = "kdeartwork";
      value = "0f531rzz5ryq9irkijlb2anxm9zl933yq7q9h9im30s2w4fx31j6";
    }
    {
      name = "kdebase";
      value = "0aj5w84clbwhckpfryhcfwjnqamdmjm6mf0nf9g1qdaa2k2sfiqm";
    }
    {
      name = "kdebase-runtime";
      value = "0fhzxhj6p8nam4zi9wz998q06j70wvk1zgrb95lblfcjvs5f8ggb";
    }
    {
      name = "kdebase-workspace";
      value = "02s2bfid02js0r9400jfk6g74b0m8y7qi95ag7pj3ynn59fz9fv7";
    }
    {
      name = "kdebindings";
      value = "0ggq2djhcf74zb3wr2g1i2a72ncxs9h787sf4136ys0frxc4h898";
    }
    {
      name = "kdeedu";
      value = "172ix71977i2rm8r2fqdwrs954d0q82xf97hzpvqmjia3hp823iv";
    }
    {
      name = "kdegames";
      value = "1srk4vh9n1wh71hamzzvsgwml2j6yi959w61wbqrx9hd0vh3nfcz";
    }
    {
      name = "kdegraphics";
      value = "0d93kg8bhg2qwbpjnkgygbc8i6qc3wmdy419h15zafy6v66hfnqi";
    }
    {
      name = "kdelibs";
      value = "04n0whn0srfnqb3gr4w4g92s2vwhxhsmik5bkja4zzkkyylpg76b";
    }
    {
      name = "kdemultimedia";
      value = "1wclpg44fips943l0yiydr8bg0r0gw1j1ji8jvivcwl8mhq2qxf1";
    }
    {
      name = "kdenetwork";
      value = "13r9fpbcsl5a12rlg1cifvply4shmvsmm8c1h6bkgyw55k6m652v";
    }
    {
      name = "kdepimlibs";
      value = "1zc07iw6f78zbbnywplavjb4vdvhr14262wfx35zw3w34x3s0czb";
    }
    {
      name = "kdeplasma-addons";
      value = "0x46l5840iy6nlfzbgi711rdrrjwaxzglsd9hjs90sswqyiid1zg";
    }
    {
      name = "kdesdk";
      value = "0dgvg0pzdwk4cg0v4rmjid0lb25r0gbhciywb2whr0l4g8rnsriz";
    }
    {
      name = "kdetoys";
      value = "1kzzwz8gbd0j9758vxb0cpklgcb9szl49lpyv98bb94378syvn87";
    }
    {
      name = "kdeutils";
      value = "1c2k8vh7vyjlvq32grs32iwfgg9q9jblm2lwhlkmrl391b38fc0d";
    }
    {
      name = "kdewebdev";
      value = "15zj9jwrqfkz94hhjcic8k58d6fwrxa6xacmxpmps80j7qgik0d9";
    }
    {
      name = "oxygen-icons";
      value = "15m08kkxyqw00zhi07mqwwkghw2knf9djw2sw32rpiicg7ppfjgx";
    }
  ];
}
