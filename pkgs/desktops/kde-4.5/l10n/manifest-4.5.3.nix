[
{
  lang = "ar";
  sha256 = "0i22kwm737ry3l3q9pwmbnv2ln317fz3r1z8x8i6l0czdkj5qxw6";
}
{
  lang = "bg";
  sha256 = "0qa2x3zz5aaxlm88zvrkglsisc85la6asfh5yiijc9hga27ykp16";
}
{
  lang = "ca";
  sha256 = "1ip55x72chxs9v1hr8rzi519xhfvkkbv7b4k4pc5nygpdqzj1whh";
}
{
  lang = "ca@valencia";
  sha256 = "0g8x685a2ndvm9qh2wp6kxdlhbidgsjacw80p488hizgqczakbn7";
}
{
  lang = "cs";
  sha256 = "19zx1zhd6wrkqmc6qnyj4rsll14difps0dzwnwa56l3h5ir1828v";
}
{
  lang = "da";
  sha256 = "1q50w3dmxbpczgxgl6n49lc5qqym42r1kz4h7ijqfazb5mwk9zqc";
}
{
  lang = "de";
  sha256 = "0ff4174szrxm4wn3jmkkzfr3jl0yqxkkqi3g3xj71mazsrsa1ghs";
}
{
  lang = "el";
  sha256 = "1r5m3d7cc3mi748ckmkky53j3cmw20pibczvkwcs2c65q6l0vcjc";
}
{
  lang = "en_GB";
  sha256 = "194jdjpqcpi861sxpvqagm8nammwksxyi55i75bw53p8x87i27pv";
}
{
  lang = "eo";
  sha256 = "1gbmqm9mgmps37pwr7lb140pvrmm24m6aw989qaig27gjjqpmvnx";
}
{
  lang = "es";
  sha256 = "1hnnk2zvlbppdqqbf1vi2d2c8mpw693bwb6dixba4p542ir7bi8i";
}
{
  lang = "et";
  sha256 = "0qgv7v93n4mnpcdvjxkdbvyaji1nhgj0nxby5hxrznnvgwbvfzbf";
}
{
  lang = "eu";
  sha256 = "1rmdak9pgkswrnwdpmgck2lq2cw5v3bjc8vv79mb3kyki901423g";
}
{
  lang = "fi";
  sha256 = "17s9yqadgjq2dmp28syxzzprya80j0b2l60r293k0bdkdfrgz07p";
}
{
  lang = "fr";
  sha256 = "15hh7k9dhi2mb27rihjc9dfii1ab8c1i2jbhihmjmb8hd5dm30bx";
}
{
  lang = "fy";
  sha256 = "0q5vcnimnvl3gmrqj2fr6i7fvw2bd3lrg77s03hf25crkfsd7lr8";
}
{
  lang = "ga";
  sha256 = "0cy8a0jvb0w35i3482a86rdnbbyswww9dlb57rjhw6mqwa3qp6j6";
}
{
  lang = "gl";
  sha256 = "1xdzhf6kwkz2r77i86grcdbrwxm37shaif2vjhhwgimplsh7z5s3";
}
{
  lang = "gu";
  sha256 = "1sky7kc5prg1bnkh2nhriqs33qa7rlfds9k31sf24x4jhfn75lji";
}
{
  lang = "he";
  sha256 = "0bkfwilsvvaiklgnfnyf6p1dyamwj310b9xkrdga7szrsfgrviip";
}
{
  lang = "hi";
  sha256 = "0fxz77pfv9ayx43pf037bpllhjwxdhsggl7yq5qg28d233602sxc";
}
{
  lang = "hr";
  sha256 = "16nkrdh1y9mk5rkdyl70i9dh8xam2fr23lz7p0qmjf66npjnvcz1";
}
{
  lang = "hu";
  sha256 = "141xhsxv2bk81sx3lzzd78a9qzp0qzrcp32i9d4bkmyppwiia9h2";
}
{
  lang = "ia";
  sha256 = "1hgzk2yfwjqils8zd1rv1djdjsj932mqxcl4bnfv3az4vl0ly405";
}
{
  lang = "id";
  sha256 = "12hqichrl7i3fjkqyhc2c3dp34bhjl0ps4g3vpc0m59w2vwyv4sw";
}
{
  lang = "is";
  sha256 = "1ydx8sx7dwdzava4y3aj0vfd23nfqiwq5m9aw3ayaliipacrq1dg";
}
{
  lang = "it";
  sha256 = "0z7zig48ixqs18dyi6q28k8dwn2a2giwsndignd5ck7xbi5h7rn8";
}
{
  lang = "ja";
  sha256 = "0mpch19nk8pqizq2zcs8jpxrsj2nq9lx82m58dwgmxdn3qlpklm3";
}
{
  lang = "kk";
  sha256 = "099hqac4a5z0aq3nw0znwjlxgxgpmmjb385nm2z74s3d8d4dw8bb";
}
{
  lang = "km";
  sha256 = "09lmpxv3ljm7bhhnmj94dia7ldz4sdnf9v0cs7ia83iiaw008hc9";
}
{
  lang = "kn";
  sha256 = "0bzwbncgzf3mhp7f24r9wrf8ip6d5slmvvycmyicvpwfj61dcwsx";
}
{
  lang = "ko";
  sha256 = "0saxxrzvcsjjw4jascqk8vpr8lm7xq737vabhmsfqga0262h8wzl";
}
{
  lang = "lt";
  sha256 = "1fn6dbncqdgnwgrd58jcdbc9c474kyb397djxk5lcf9gmdq7634q";
}
{
  lang = "lv";
  sha256 = "0vml848hxynic4cg3w8zadmfxs7l0qqd7xb3c7kxznrqgkra7rqs";
}
{
  lang = "ml";
  sha256 = "1d77wb1d7lsmpv2vh8jzfi4y30bd5nn8ljlfx42yfdyw9drkbm38";
}
{
  lang = "nb";
  sha256 = "1mprsawqaqb6i2nhgbvq8hp1nmfwlxvd9chfx49xas7wk209jd25";
}
{
  lang = "nds";
  sha256 = "005i6vvq6s8c9spjx0is29jj9d0x4lm766njvsgsa0mrzgfps6w7";
}
{
  lang = "nl";
  sha256 = "0f0z2ql08pgdbqig67jjxk8p1bf6fk70br9nhmsk72k2lv7abb0y";
}
{
  lang = "nn";
  sha256 = "08ajabbzrng3gn56bl0wk15rd43pwlrd0crj7c4n8aq61qgz1c1p";
}
{
  lang = "pa";
  sha256 = "099d7fhk48c3vra7bcam89ffs86iw495vxycr3679ah1bfwf8k68";
}
{
  lang = "pl";
  sha256 = "1qf6bycvzz6p0lvvkpq2lwvjiln42xrq0qhjzcb7m8zs1zan47hi";
}
{
  lang = "pt";
  sha256 = "16726ggm87y3bvyhdpvvwcw6n5s8dw5bjw5kxpz7160iljabn71a";
}
{
  lang = "pt_BR";
  sha256 = "12fp3gf0iicwv426ni7n45rbxywrv4ni73c1ljw1lahfdhmhkxsv";
}
{
  lang = "ro";
  sha256 = "0bi25kmg7jcmz8c6xlqadzv5jr6kxx8z0pbbhgbh2nl6yfjw7fm9";
}
{
  lang = "ru";
  sha256 = "0g7ibrv5x9dlhawjcmd302yg4vql31675w7cnzjcpkc9sga3i764";
}
{
  lang = "sk";
  sha256 = "0jmndf6kxqhqgfiilzfzaf9hgq6c66ndcw1lc306kcw0qv90fm1s";
}
{
  lang = "sl";
  sha256 = "1lic93q78kq831pzgzmydy6x9fgzkyrwpl5958sjpvbf2dra1xmp";
}
{
  lang = "sr";
  sha256 = "0g9cdkq7lcm2gbgsk001ywpwjwbsi8xxv90cklifgxfvqnd351gw";
}
{
  lang = "sv";
  sha256 = "0l7xqyya76np43acq3hjgvcipz0acc2iwqmwjs2hgnhbs12fny2c";
}
{
  lang = "th";
  sha256 = "1pbjxng4p35zd2aw78x3z5ycil1diah39bknmdgpily5cd3g3bg8";
}
{
  lang = "tr";
  sha256 = "1spsr67k9wmcaypk0yzqg60qq6ynw4xfi3xcsdm2vkhgcc9rk8cl";
}
{
  lang = "uk";
  sha256 = "1jii27q1m0sj3vk7ly45m9zi49l4rkr2v05z8zzfbxi2gzg02kc1";
}
{
  lang = "wa";
  sha256 = "0mqpqpwj1qnzrqyd4lc0fkhj1r42p228kg2yyy6xn2xa4na8sg2w";
}
{
  lang = "zh_CN";
  sha256 = "0hx69djrd7nvm889b5ig2hjy73m0vhzxxj7njrh21y551pcj7s0q";
}
{
  lang = "zh_TW";
  sha256 = "1axpangs004scmhq35m22axg3v6rdpidndvr1i49cxf3bhgpqpqs";
}
]
