[
{
  lang = "ar";
  sha256 = "1yn5dvd2d6wk12434x7v463ywz0sn1d9z2fhsbhclwzns3p07brn";
}
{
  lang = "bg";
  sha256 = "0dnhywbld5ll4wvpl42kn6bnffjr72d4wp36446ffnwkqvhwg0bm";
}
{
  lang = "ca";
  sha256 = "0i6cplf7fsr4q6yyv2y4sj1d44zd4d81h3mh6fbc0icq42im4pyq";
}
{
  lang = "ca@valencia";
  sha256 = "1p55xn6k47ag4ph8byaasp27sczh6asqj0x8677hacmm01vps4xy";
}
{
  lang = "cs";
  sha256 = "16yz0yc8b0g2kzpywi4a0xys6rkj47n45l3i4f39ycypxavqya0v";
}
{
  lang = "da";
  sha256 = "0pc4im8rd4n6wn65gwyiyl4c414kp1gspw45xzj3mssbpghhn0ic";
}
{
  lang = "de";
  sha256 = "1apdjmyb3f07rjld43yabr3bvvb8xikhlzr0zmv37y43bzbvlk9x";
}
{
  lang = "el";
  sha256 = "1irh0pjaxfx893792s3wl0k8m9cy6c428isdlnij1mdbpyg4pw9d";
}
{
  lang = "en_GB";
  sha256 = "08248znhd1pxgx6izi8pfrccm31fm083bq4i9d9f4m3b9svxj63i";
}
{
  lang = "eo";
  sha256 = "1ydizv2sggfdwlh9vlf9dzf7pwjvqj3aa1vx1wdb1qzp913kixy9";
}
{
  lang = "es";
  sha256 = "11545n83bknw6a1h1dw3sxlswhqj8mm0nm9nricrmjwxhi2r2gma";
}
{
  lang = "et";
  sha256 = "1dpk4h5yfg5ljf5l6yg5i8r4v4k5cq4dq8bdir883lwlkvzmql5j";
}
{
  lang = "eu";
  sha256 = "1qfhqbpxh1mgxbzh7l8gb0xsm9pi50mq48ya32s8a502khwwxh41";
}
{
  lang = "fi";
  sha256 = "11a03s37sd1fn0s0yx8lf369l8da3qmf6aqagrlsn3cj9rwa3pn6";
}
{
  lang = "fr";
  sha256 = "0f739jlpxfqyd41y0l5ydmdjwmn3ihvcxihl3v2cmwyaxfhvapbj";
}
{
  lang = "fy";
  sha256 = "057n58mnryjxqh3v9vyf5qysf6xilmiw1p6gnsdnmqvznccmx7j7";
}
{
  lang = "ga";
  sha256 = "1wbc9mxj7yf630x3dqabv783dv012k6xna362a7cy3pzbdr49kl2";
}
{
  lang = "gl";
  sha256 = "0vg248wbmfmcdskq60nafnkly9l3i32yz21xbwjmnvs2drwphayz";
}
{
  lang = "gu";
  sha256 = "14misxmdi5v26fql40vqwjci93l5jrjqrwd049pj5i0jnrjhy5rd";
}
{
  lang = "he";
  sha256 = "19h2ca93kqr293qjg0gy5mpgw0x3cgdmjs70k42781zl4xz1prjp";
}
{
  lang = "hi";
  sha256 = "0qhp5ml6jindbxds8mllnsyi5c0mlnmwppfdxjapg0m229f6dgh6";
}
{
  lang = "hr";
  sha256 = "08i7lg3rii1lciy42ny65k43sn4lv85jl3464rn7a91yvbk18iym";
}
{
  lang = "hu";
  sha256 = "0wfxqcjpi0yl7xsnpb5k25jl0c7haf9dr2f85wmw6iv2iamdkfmh";
}
{
  lang = "ia";
  sha256 = "1ixqr026g4fi7fbrr7wi49h3662b3jdb8nsfqs5dd41mjchk20c8";
}
{
  lang = "id";
  sha256 = "079n7q5nx5mass01dqqngvajjfs4qdcdl9d9jdzkn4fzhffpgdap";
}
{
  lang = "is";
  sha256 = "150snwlbyv7461bh6yfl8cq5dv5alxf37grdgq2zynkpnx9i3s8k";
}
{
  lang = "it";
  sha256 = "07bza0by629gywqxkxchzvlw8fs1swg7ivpr3zn48lm8ka9xqx3s";
}
{
  lang = "ja";
  sha256 = "0bpjahc276wm0fscnlffqq2m838jkir17s4v12q0zr0dmryxd7lc";
}
{
  lang = "kk";
  sha256 = "11cyklk2j6igmq71j7fcr9gg8jn99lq6z9yi77p7hn4rid7dflgp";
}
{
  lang = "km";
  sha256 = "0d7f7zz6ys3yfq3wlbk621yy7zzblzb3zv8g849mbpb5m5rl6bsg";
}
{
  lang = "kn";
  sha256 = "0hcri8am13sy7qnl0459b7cphb0jw4fc40nqw3crrdrsr8677abj";
}
{
  lang = "ko";
  sha256 = "0zz6k2xgfvb80031dinsyp6crgr1iy0iv1d0m2bq1985aa98rrih";
}
{
  lang = "lt";
  sha256 = "1knzjdhl98h8jdad5dwkqkiqs7wwib57jvs3jf0zacvqbcpchwb7";
}
{
  lang = "lv";
  sha256 = "0pm9k4ww10dnyvkgkm6n8cddwlj5431k17sbshgmgjnw9bszk798";
}
{
  lang = "ml";
  sha256 = "06j6124q2mmpjs5y3gqy7fxgldwnvq5683394s3m23n8q81yg0wj";
}
{
  lang = "nb";
  sha256 = "180vsfx87ynizzga878757qjnj1k91qad57iisw5hqcxk2fg3v22";
}
{
  lang = "nds";
  sha256 = "16i9wz1lzkqij61raffm8h4l6fjri4h9ybbgxy7nlqiqaj67jghl";
}
{
  lang = "nl";
  sha256 = "1clrmccm2vdvi9v7f2yqhhf69iln57m7hpw2638is6fs31ivyr1v";
}
{
  lang = "nn";
  sha256 = "0447v1wzbb7bvmpbcyav3gv45vv4zl9p9nj3sk45c8md1by82hc3";
}
{
  lang = "pa";
  sha256 = "1fbrfczjp2yliz13swha2f4767jwzy9bi1hb1dqi2yjhyqhj7k6p";
}
{
  lang = "pl";
  sha256 = "1xzsfrxwgf6kmfa1j5dy699rxaxmc3vpcv8zgqi96kwwhqmmwn9p";
}
{
  lang = "pt";
  sha256 = "08gdvkbqfshbgf6zwyg05g05kc7hm81s01z7p67jc2q80vkl7ahz";
}
{
  lang = "pt_BR";
  sha256 = "05x600wzp7x1hx262pmb27js146j6sv9c3mbw748q1l3c7cdp0xs";
}
{
  lang = "ro";
  sha256 = "13r08mka2j6zyz4a3ycs57zaj4ipjdv12sbag0bjwk9dx5x45xs5";
}
{
  lang = "ru";
  sha256 = "0yw8aax09yl89ab15rlc66lmx4x552hkg5fd89vnhlbk2h9v1rms";
}
{
  lang = "sk";
  sha256 = "1k905nmvbig7qmbq83y9jbgp72vg9l1mj2iahqz4r726zpqz4fxq";
}
{
  lang = "sl";
  sha256 = "1ywy35wj8aa17q7hf692dwi7s0xi8ns9p0pmfm13nz8214rbmc4q";
}
{
  lang = "sr";
  sha256 = "0rqycg08wgrcvphd4mxjsk460nflpv78r0wil4bsqfa8i774v8r0";
}
{
  lang = "sv";
  sha256 = "0rl2hdnhn9r5yk4zp1sxbxhqchs411kffs5krdym19a3kl83zp36";
}
{
  lang = "th";
  sha256 = "0n62zs6sjfdkyxs7n6hmji8bzl9y2r4gsqs0jysmjkz23y6160bh";
}
{
  lang = "tr";
  sha256 = "0phc1cbp98bilvscxnxnbzwgk85ilws5l3n3bsgbkqrh5rqz9fyn";
}
{
  lang = "uk";
  sha256 = "1hy7hriikvmpjgmxf1zw2j0hkv6f8ig8v20damcivkid3maj307g";
}
{
  lang = "wa";
  sha256 = "11zwqkv1qpqgxxqvq0y92xyq88czzyx76krx1qbc0d50mv5zk91r";
}
{
  lang = "zh_CN";
  sha256 = "0pn3xd91ka20mf35rsdyf9sp3cwf9dpxn7lradz2380mkira6mp0";
}
{
  lang = "zh_TW";
  sha256 = "0zx879d0fkgblfq6574kq4wdhip729ni8v1cjhg0v97l7pnridzk";
}
]
