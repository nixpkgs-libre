[
{
  lang = "ar";
  sha256 = "1wlg38gfv77d4rfzr6ba8mcrbq1lsgvqpd1zgcfgv9m6c5i32rh0";
}
{
  lang = "bg";
  sha256 = "0dy2zirfs2aan445862srfrxkvmv9l6dk1qzk2mis8lmq7qnvv4x";
}
{
  lang = "ca";
  sha256 = "0ay15k75d44ggr6s5qxip7cwyddl91zlqgwnp71iw720iq49d4c1";
}
{
  lang = "ca@valencia";
  sha256 = "0fmcmpm3wi1qfd0lvd10d0crn7jfzf9n184x45fvqw4dkn62phdc";
}
{
  lang = "cs";
  sha256 = "0r06qh0i3il2n07bbp70gsr5p7y5gb9gd1gzy5m5g33mmnhgf2af";
}
{
  lang = "da";
  sha256 = "1dz2ixm42ndyqwjs6h3900ycq9j50l0nzahxmjfm76b9r2cbph6g";
}
{
  lang = "de";
  sha256 = "1729hhfwszairx08x0s5rbidrbxqqvlh1m6rww2j1bqv5krgadns";
}
{
  lang = "el";
  sha256 = "1is7vy7m0y3x6n5llcg4fz8qd6ddjvfxriyggw9kx1g661z25gny";
}
{
  lang = "en_GB";
  sha256 = "0nwgj99y13h3fc6ljmnmrxaj3aiwg031p4nrb49ngmh66r1c3n2r";
}
{
  lang = "eo";
  sha256 = "0w78f6n2nhalgj7i997hrkka9i252mrny8mgnq8bfwsr03lfjs9h";
}
{
  lang = "es";
  sha256 = "18cqvjw7f9kj0inhhw00b6jxgsikamr0iik7pbblra2shimpr9hh";
}
{
  lang = "et";
  sha256 = "1zrdywhc34kj09141ybr5g98bf1zqrxzazsdi51ln07pcmbrn7fd";
}
{
  lang = "eu";
  sha256 = "03ff2narllyjyl7ddqq80i88m10ma1g9l0k970vpakw2bk3qd90c";
}
{
  lang = "fi";
  sha256 = "0kpzsngspxgfgy2fdghb7syksrs4h3392xngl7g1ndr6m9wfrl33";
}
{
  lang = "fr";
  sha256 = "042jbsr7agkikyj4f3wz6vbwgalmkq75sxm9mr0hspaxqw0rdww5";
}
{
  lang = "fy";
  sha256 = "0yvqi4z98aql596pa77blrj34mxhv3q04nbp4mdiss9i6wkqxa56";
}
{
  lang = "ga";
  sha256 = "14s90f9nlk2knkf8lsxklkval39gb77ca70hcs4n571hl3hdvpd7";
}
{
  lang = "gl";
  sha256 = "0xh409ydzqzsbwq7727i5yj6irb2y75maa7kkvm9bmhpb5whn0k6";
}
{
  lang = "gu";
  sha256 = "1agly26l4z5lc0s1b4j5rdpdk9sf0ffw9zp8j4mvaz501aifq777";
}
{
  lang = "he";
  sha256 = "0z2czis29bqga9jzgrdzqf85f6k14gr1rcyawhc6ddffmb5is8pj";
}
{
  lang = "hi";
  sha256 = "0f1j6682pfajg7dlf2rqqif5s8w00wpjr688kchqdsld7byax4wf";
}
{
  lang = "hr";
  sha256 = "126f04zgnha153yzmq5263chwiz2lv910sgrh23a07k6piip20wi";
}
{
  lang = "hu";
  sha256 = "10cwzpsq42vkyb4x7sc406pjf3ryr5api0cr9vwhnnqprwr82yw3";
}
{
  lang = "ia";
  sha256 = "0gd6li2iy1l0bg9kvs19hsxlrqzfa92if7j9g90l8qml1xg0d916";
}
{
  lang = "id";
  sha256 = "1rz8h2yna7hyvv2bynf0q5zz42qs74y6z4y2ca92b0qfc4a1ai1d";
}
{
  lang = "is";
  sha256 = "1lkr67dlpskj141y7wihxdgnmkag1dalb8s0r1xixf051ax2wd2y";
}
{
  lang = "it";
  sha256 = "1pz0mlg1p9mrqb79m7dsm6lk8iicyc8jwbrvrhlsji6ahr81pybk";
}
{
  lang = "ja";
  sha256 = "12333ilhd67y8gkddpx7jzdvs10h66qi8qi4nrqb6ici6spxmb3i";
}
{
  lang = "kk";
  sha256 = "0q845ix7kadfsphvfdkz7fawh3zqmainsw321mn90n6izf8y893j";
}
{
  lang = "km";
  sha256 = "0ahmw632kqclplq5d65inglhvn5jzxdjhhpkqx32rq7sxk3qhhrf";
}
{
  lang = "kn";
  sha256 = "12lp3rbcxbqm27clm71vli2pdwyp8r2bk3a2mmvnv016pjn0vphn";
}
{
  lang = "ko";
  sha256 = "12fb0wqms571xp9mnyiv61khby71cwsf9sg05w0gjci7avwxv6w0";
}
{
  lang = "lt";
  sha256 = "1p2rg1w3b6qgzjhsmi8dsj3mh7n8wbkgp9hrwbsl8dj7l6yz9799";
}
{
  lang = "lv";
  sha256 = "0zm48nl504dwwsrn8b44ldh0dzj7j42v6r54bsfsqajzq7f857yi";
}
{
  lang = "ml";
  sha256 = "0wxsfykvsg1iz0az1jngdl26sgs73pv0si30n0835xzclcv4dflx";
}
{
  lang = "nb";
  sha256 = "1040i62nh9ldv2v5ps483r6ccp2jy6w94kijv5h01k2p82iv8f85";
}
{
  lang = "nds";
  sha256 = "1ljgdlrqwb966xpxg2fpjcqzmplhdbmd566q7k69x5fmmyxw7vn9";
}
{
  lang = "nl";
  sha256 = "08vwq4xy392bqln31lvbdgzhx50sj44mrnw920j6k2a0f1qmkar7";
}
{
  lang = "nn";
  sha256 = "1da0xrbx8p72lp6r4nrhh6q5sgfm08vak6dk9g3kssjgnjarg785";
}
{
  lang = "pa";
  sha256 = "05jkc60csj16gxxh6z5n15g65yqf2nd77sa8f00vjhx58iisvvb2";
}
{
  lang = "pl";
  sha256 = "0hi7dsrg7dln9nkfc1wichmn334303qxdkihv13fybw43xp3gznv";
}
{
  lang = "pt";
  sha256 = "17j0angzvv2mxj12jb7vv3dlmljrfhyki5clmj040z64siam0w4k";
}
{
  lang = "pt_BR";
  sha256 = "1c6i79bslyzz0skfh05152jxn5hqlbas2l9fgzh58amn5bgy6afg";
}
{
  lang = "ro";
  sha256 = "1yljfz3b7lnnlnwvqf71b22ss99an759hg3xky1nzq11kpp9a5y0";
}
{
  lang = "ru";
  sha256 = "0p1zp2bgz3afxq4r6cbr1sv56ymq0xvn249vi048knsa2sclq8bk";
}
{
  lang = "sk";
  sha256 = "1v6lx4jb78k44xgfdxih1mf1817piw1igmvkc39l7r8ci0drvdjx";
}
{
  lang = "sl";
  sha256 = "1xqyafbzas460p7vxvydw8k8gzsgaxsidy19hzanvdb5ns4j8fs2";
}
{
  lang = "sr";
  sha256 = "1yznci299164l89dq9k5wfahk5aw3y61i4bf08qn6zi5hmzsfv66";
}
{
  lang = "sv";
  sha256 = "1k5b9f9i5zn8j5nkafw4cl42rvigssz5fqmy0nv1awj87r9vd3x6";
}
{
  lang = "th";
  sha256 = "0l25jags9kfhnr0v8k5id2pl85rlxly5a74i94wzvnf0abkmy6hi";
}
{
  lang = "tr";
  sha256 = "166rw0znk906zikdxg573xykfxmwdnld2ps9c0y758n6mvl107xz";
}
{
  lang = "uk";
  sha256 = "1hlx3dnf3avcnfs55v0vq9knmf001hs00fd4rqigx8y09i8947jc";
}
{
  lang = "wa";
  sha256 = "17i73mmc1vwvjwrvz9sq45q0ax43xq3agi2d95s772kpbrf4l8qj";
}
{
  lang = "zh_CN";
  sha256 = "1xqi0rn36dm7f4j5x9gdk6ml8y3vzzm009qp8algqrzy46j864cz";
}
{
  lang = "zh_TW";
  sha256 = "1qh3vnpk6ickdqa8n9h1na16s6q7kr61f44b0qymxz2lw79ihhgz";
}
]
