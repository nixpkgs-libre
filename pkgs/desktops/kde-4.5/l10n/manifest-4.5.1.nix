[
{
  lang = "ar";
  sha256 = "0xgmrwlza3p2fvj66y4vc4r81r5y2n2igjaj2xmgvb2pvh4zx0ws";
}
{
  lang = "bg";
  sha256 = "1npa9y8i7j68c8sa9cd96gy4n65bkl474agibax8rdk2mniyqrn9";
}
{
  lang = "ca";
  sha256 = "0097sk50cf1cq6wnah3kiab8vkh68snzahcng2blk63gw908yfvx";
}
{
  lang = "cs";
  sha256 = "077424cbqn68rf9wb026ip1n0rq5lz1iwp4k7ih15j87ymbrd03f";
}
{
  lang = "da";
  sha256 = "0p80a2pn9hgih2y5s27z8ir8jkyz68i87sdw439c8z3pjww5dy2h";
}
{
  lang = "de";
  sha256 = "007r6wf0in22y2jc1nr1msazv6bd48136kc4q0633gn1igdhb03h";
}
{
  lang = "el";
  sha256 = "1ffgqjxgdfsjx0nwhj5yl2pn2kmc92s01wfxzdvb6kss9r53v95k";
}
{
  lang = "en_GB";
  sha256 = "1bmg1aygaarnpka9dfvxgnzwqphqaly8rfps63g03ncls7ml8ilb";
}
{
  lang = "eo";
  sha256 = "0bqrarah2pc6kjcfla117rwc0k1m23ndp4nm4ac2sygmanz1j6bk";
}
{
  lang = "es";
  sha256 = "0hr3halsjjdzlmx2rfxycacm26pxs1f3m8awahmr1dp5ngid42wn";
}
{
  lang = "et";
  sha256 = "1pj1wjnjjj052csn0ajql2i3ynpd4fc9l53zj4j3vsd177gypdjw";
}
{
  lang = "eu";
  sha256 = "0zhrqy8b7wn8vdq75dlafc91mhvqpn3cw7hn90jj99c1aazdhjpd";
}
{
  lang = "fi";
  sha256 = "0x0dhy1c7wn5wxqx2v7jyv10rc6gi6klmw0q80vp3xpfma9l16yg";
}
{
  lang = "fr";
  sha256 = "1yvzpvxdzznrj61hdbi9jgpxdb9l0i8g24q1xkswpnp87jrncpaq";
}
{
  lang = "fy";
  sha256 = "14c740yfw5v043rmqmwnb3ic0s7l2kcjy57qdqhfpmr75m4rjrvl";
}
{
  lang = "ga";
  sha256 = "0w5cj0bgyy2pna8hqcpx82n3pwzr9vzshzknz86azb8c0pwkcjdw";
}
{
  lang = "gl";
  sha256 = "00kwqzypkw0bi8nhz75a367h2filwkmc1kiiv66l2s3d4vrkdgjz";
}
{
  lang = "gu";
  sha256 = "0as072rz4k4h8b0ix8jipn6bfck8van024sz1gd6xj1bhs83wk0m";
}
{
  lang = "he";
  sha256 = "1f5j1qgkmzjagmyjxa00xmfxk5zgfms77gidg88hycrvgrxm6g59";
}
{
  lang = "hi";
  sha256 = "0sy6cis0hkddmq4jw43a7rpxjadmzrgbgyhnnhyp9m6n225rh54g";
}
{
  lang = "hr";
  sha256 = "016h7iqfkarl6p20gqcajknw3bsbqvby824fsdyrrhan82vsqd0k";
}
{
  lang = "hu";
  sha256 = "1ck6vrrvvwlcxlgmmjq4hk5dh38clqvf3fwc7ndgr4wrxlr4c94m";
}
{
  lang = "ia";
  sha256 = "13mnd7ndkfdq591da3cwj64wchq86qk265i6r7xir87gxn5m86xr";
}
{
  lang = "id";
  sha256 = "0a1rw0mx1fdmps5vfx7m8mrmsqvqkpdbpbwys9sxbxy1zcbinasr";
}
{
  lang = "is";
  sha256 = "0hinhxsq4nm4g97gcdfyx8fzx6d0syg9llww9spwpqp26myad69r";
}
{
  lang = "it";
  sha256 = "0z4kdlmyliqa6qgdxbimifp535p6nvvydpa504i9klxcywc382y5";
}
{
  lang = "ja";
  sha256 = "1vsyjgwh69wwadn27ncxmr85d1qn91xvrbfxxl8x9gqa44dzrs7k";
}
{
  lang = "kk";
  sha256 = "1nzx2djxav6hga6ha473xkqiqgv8ikh3iq3b7i20zqcsjp3lhynq";
}
{
  lang = "km";
  sha256 = "0rd36apjlgq3qvhbdq0a289hbn1hff1qbnphaisy1p1dsh3bbnw9";
}
{
  lang = "kn";
  sha256 = "0al7whnmcpiq50b56xhzxpd6zmwv79s5j62m6z537kl3ldq9is5l";
}
{
  lang = "ko";
  sha256 = "0s5y6h8nx4xp5vd39qpby303zsvi67yrbdzsgvig98gbw61szkd7";
}
{
  lang = "lt";
  sha256 = "0wmwfn7w6z1lvjgn283rcpxv8j6lh8yzkvgjmhdg2r4ys948gzz0";
}
{
  lang = "lv";
  sha256 = "1w4k7rr06m0ynl52gdikqw45754pj7z9n65pxyv88gxj5sjkzlw8";
}
{
  lang = "ml";
  sha256 = "1qgs6xwyx1sc496ybq1m1mm0wyl9fz6317p4m5jvwiicldrcx3hq";
}
{
  lang = "nb";
  sha256 = "0frnnxang8k491cnmn9lc2kbqkqr5m40c53d75l07j97jndv98y1";
}
{
  lang = "nds";
  sha256 = "1p3h86qzrrhxrvl4qryizm3j6lnq3lm1vpnnapkk04y7msz0fdpr";
}
{
  lang = "nl";
  sha256 = "0lphl2j0faalaj3mkkqv5806bg2fd6s1478cprrrkw788hvvlmzl";
}
{
  lang = "nn";
  sha256 = "06pj24zfsgbkk1jxjjs5by8czv0llfz90iq6rq4lxnc0s195isjg";
}
{
  lang = "pa";
  sha256 = "1sqqanvxzw63bdlkgvi6jlngvrshqrfl8d0jac103wvy4y4f6l5r";
}
{
  lang = "pl";
  sha256 = "13n278n3p32bphch54x7qgba268m95kmql3qgxsx8ziy8riir9mm";
}
{
  lang = "pt";
  sha256 = "12mkprpk9imda0iyi65mg4rs3w2qnyq8iszg0k7yqf7537zpv4x1";
}
{
  lang = "pt_BR";
  sha256 = "1ipn6kvc3nq84fvpb9xrpwkqjaj2sya3y1cr2ai51909yfjk5r35";
}
{
  lang = "ro";
  sha256 = "1kfyjlpkjnmv7fryr1w2a9d1dijkmhs3mqpqg0hi7316qy2z10jy";
}
{
  lang = "ru";
  sha256 = "0m62rszff2k4rffmy3h9hxhs8bf523cfinlyay663zq3bh35i6h9";
}
{
  lang = "sk";
  sha256 = "07pzslmln8yh0s8ik0bj86rhyy4w21w13pvf9z6ifapqigklw33c";
}
{
  lang = "sl";
  sha256 = "1vrdzbl5ii6paaick88dkr5fabjpi36gjwwz67kqy6d9nm44jjp1";
}
{
  lang = "sr";
  sha256 = "186ncq0hr0lrn1l6xhqwjwqrmd9xw9qa0kf9cqvj0x3j6qk4mjqf";
}
{
  lang = "sv";
  sha256 = "0r62fs75a1kfr2dsfg1zrzh0c2wgbv6jq4c1w05wcbmqvpq16ks6";
}
{
  lang = "th";
  sha256 = "1apvlcmbwhnjc3dlfjzah1ckwwhjn2rcrvgmv1rwvn9py7yfzhf5";
}
{
  lang = "tr";
  sha256 = "0zci2fqcnlj9y23sy7v8yb1nc37dfqbpj1xrfs7sfvhj12048sb0";
}
{
  lang = "uk";
  sha256 = "0p9jdz8w8xiiqhbsj90gkshcpijh2l8hfbvvqy12d1nx5wb1glp3";
}
{
  lang = "wa";
  sha256 = "06hybn2l2jlibxr6lg91p0p7nnc6lyisx0w64zar52h958mqa9p8";
}
{
  lang = "zh_CN";
  sha256 = "1f786n41yqfi02gd0n9lf30xn9djgbm26c8kqx8p7fm2b5vw28jh";
}
{
  lang = "zh_TW";
  sha256 = "065d2ggahphsffk7p8xb9p5xzm12azz84s9jll447sawwks0jgsg";
}
]
