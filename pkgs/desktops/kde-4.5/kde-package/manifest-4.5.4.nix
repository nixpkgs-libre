[
{
  module = "kdeaccessibility";
  sha256 = "0jd07n143r2610a261xx193zkigs30a4p645pzyw3dsi6la8ms0r";
}
{
  module = "kdeadmin";
  sha256 = "0frf4clljqnynxv67hqypsfaifdrgbgc0zb9lcckjialc03f2kfm";
}
{
  module = "kdeartwork";
  sha256 = "08260bml12y3xwr61q2qxrp0aqcshi14h7n93b12q0m9fs8n87x0";
}
{
  module = "kdebase";
  sha256 = "1k9hla4qwrgz12g4n72c46w6b1srjnwf95zxhj2axqdw4k4hfj6z";
}
{
  module = "kdebase-runtime";
  sha256 = "0rqrx9hz266dc9l7sn2pakqy36w5919gchwc4ghb5qzira3jjg1h";
}
{
  module = "kdebase-workspace";
  sha256 = "09a4jsa3w4v4ldsh244isbbrsv350xcmd2py0sb3lvja7gf9wqhw";
}
{
  module = "kdebindings";
  sha256 = "0vx7fhg74g0b2xcaxjybxci34kyc10h1i29qsdqr1005j01cdvj0";
}
{
  module = "kdeedu";
  sha256 = "080pw86l55jfhdxm3a18qh4h1p7k6w3cb37g8ymfsyx3k3diil6x";
}
{
  module = "kdegames";
  sha256 = "12p209n673fxnm9wsgc7by46z4hs3d7b3hzwgcxggzag0kyhx3s5";
}
{
  module = "kdegraphics";
  sha256 = "1b4n1ss5pg213blqrkbk0pqchfr336rybqfkcb8scv1blx0w83qs";
}
{
  module = "kdelibs";
  sha256 = "0c4ybrpdw3dblv68mj6sa7q13kx1z64f8lpakxf6f7vw1ish2dph";
}
{
  module = "kdemultimedia";
  sha256 = "16k8l5h4m2wjpzpzflk2agmg48m1gj4fyzjp67z446lmb1m8jap7";
}
{
  module = "kdenetwork";
  sha256 = "1g0hy92ixh2nzvasjm4ms8n8jyy777d909gjv16ph8g5w2pxj61w";
}
{
  module = "kdepimlibs";
  sha256 = "1yc615qf3qihlj69glm4amdrbck33vagp5xmgnp6mny5vhvdc85b";
}
{
  module = "kdeplasma-addons";
  sha256 = "0q9r8lafc3aa15smaj1r5kcyz2jw7lpbnp6qxanllfbv7c4b37nb";
}
{
  module = "kdesdk";
  sha256 = "1ia336cfs42h8b3jahd9hb2ynahyiccx6y8dfk420xvyy024i2ra";
}
{
  module = "kdetoys";
  sha256 = "12755p77r5fv2lfr78jrvcx8vg1mnsfsb1g8hpr6b59w5hh2k2mm";
}
{
  module = "kdeutils";
  sha256 = "07g96bwy8h4ydj8gdnm1cs7vgm96s3m9c2d36d8r3w6apvh5pjkc";
}
{
  module = "kdewebdev";
  sha256 = "1ng0rbrizqmqm4l0j9xahf0dwh5674cimzkzrlgj09dl4a603xar";
}
{
  module = "oxygen-icons";
  sha256 = "0j9d4h3nl3vqwh4fi1zysahgvwd0xswqa25p8jl6hl86rnawhjcy";
}
]
