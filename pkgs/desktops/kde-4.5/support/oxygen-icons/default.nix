{kde, cmake}:

kde.package {
  outputHashAlgo = "sha256";
  outputHashMode = "recursive";
  outputHash = "1hn65n4nznbp2ikbvyrp9ncasc6y3nxhi33x927vg01pp8frn4q1";

  buildInputs = [ cmake ];
  meta = {
    description = "KDE Oxygen theme icons";
    longDescription = "Contains icons for the KDE Oxygen theme, which is the default icon theme since KDE 4.3";
    license = "GPL";
    kde = {
      name = "oxygen-icons";
      version = "4.5.4";
    };
  };
}
